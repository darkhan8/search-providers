from views import provider_a, provider_b, Airflow


def setup_routes(app):
    app.router.add_get('/provider_a', provider_a)
    app.router.add_get('/provider_b', provider_b)
    app.router.add_view('/airflow', Airflow)