import db
import json
import time
import uuid
import aiohttp
import asyncio
from aiohttp import web


async def provider_a(request):
    provider_a = open('response_a.json')
    data = json.load(provider_a)
    await asyncio.sleep(30)
    return web.json_response(data)


async def provider_b(request):
    provider_b = open('response_b.json')
    data = json.load(provider_b)
    await asyncio.sleep(60)
    return web.json_response(data)


class Airflow(web.View):
    async def get(self):
        start = time.time()
        async with aiohttp.ClientSession() as session:
            async with session.get('http://0.0.0.0:9000/provider_b') as resp:
                print(await resp.text())
            async with session.get('http://0.0.0.0:9000/provider_a') as resp:
                print(await resp.text())
        print(time.time()-start)
        search_id = self.request.query.get('search_id')
        currency = self.request.query.get('currency')
        #     sort json list by key
        #     data = sorted(data, key=lambda k: k['pricing']['total'], reverse=True)
        # async with self.request.app['db'].acquire() as conn:
        #     cursor = await conn.execute(
        #         db.search.select().where(
        #             db.search.c.search_id == search_id
        #         )
        #     )
        #     records = await cursor.fetchall()
        #     # if records:
        #     #
        #     search = [dict(q) for q in records]
        return web.json_response({'search': search_id, 'status': 'COMPLETED', 'currency': currency})


    async def post(self):
        await asyncio.gather(
            asyncio.create_task(provider_a(self.request)),
            asyncio.create_task(provider_b(self.request)),
        )
        async with self.request.app['db'].acquire() as conn:
            search_id = uuid.uuid4()
            await conn.execute(db.search.insert(), [
                {'search_id': search_id}
            ])
            cursor = await conn.execute(
                db.search.select().where(
                    db.search.c.search_id == str(search_id)
                )
            )
            records = await cursor.fetchall()
            search = [dict(q) for q in records]
            return web.json_response(search)
